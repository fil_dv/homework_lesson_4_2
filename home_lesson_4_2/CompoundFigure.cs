﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    public class CompoundFigure
    {         
        public ISimplePolygon[] _arr { get; set; }

        public CompoundFigure()
        {
            _arr = new ISimplePolygon[0];
        }

        ISimplePolygon this[int index]
        {
            get { return _arr[index]; }
            set { _arr[index] = value; }
        }

        public void add(ISimplePolygon obj)
        {
            //Array.Resize(ref _arr, _arr.Length + 1);          //???????????????????
            ISimplePolygon[] tmp = new ISimplePolygon[_arr.Length + 1];            
            Array.Copy(_arr, tmp, _arr.Length);            
            _arr = tmp;
            _arr[_arr.Length - 1] = obj;            
        }
        
        public double AllArea 
        {            
            get
            {
                double res = 0;
                for (int i = 0; i < _arr.Length; ++i)
                {
                    res += _arr[i].I_area;
                }
                return res;
            }
        }
    }
}
