﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class IncorrectValueForTheFigure : InvalidFigureException
    {
        public IncorrectValueForTheFigure()
            : base()  { }

        public IncorrectValueForTheFigure(string message)
            : base(message) { }

        public IncorrectValueForTheFigure(string message, Exception innerException)
            : base(message, innerException) { }

        public IncorrectValueForTheFigure(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
