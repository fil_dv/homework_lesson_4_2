﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace home_lesson_4_2
{
    class InvalidFigureException : ApplicationException
    {
        public InvalidFigureException()
            : base()  { }

        public InvalidFigureException(string message)
            : base(message) { }

        public InvalidFigureException(string message, Exception innerException)
            : base(message, innerException) { }

        public InvalidFigureException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
