﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class Triangle : Figure, ISimplePolygon
    {
        double _a, _b, _c;

        public Triangle(double a, double b, double c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
                throw new NotPositiveDataFigureException("Sides of figure must have positive lenth only!");
            else
                if (a > b + c)
                    throw new InvalidlengthOfSides("Wrong data of sides lehdth!");
                else
                {
                    _a = a;
                    _b = b;
                    _c = c;
                }
        }

        public override double Perimeter
        {
            get { return (_a + _b + _c); }
        }

        public override double Area
        {
            
            get
            {
                double p = (_a +_b + _c) / 2;    
                return Math.Sqrt (p * (p - _a) * (p - _b) * (p - _c)); 
            }
        }

        public double Height
        {
            get 
            {
                double p = (_a + _b + _c) / 2;
                return 2 * Area / _a;
            }
        }

        public double Base
        {
            get { return _a; }
        }

        public double AngleBaseAndAdjacentHeight
        {
            get { return Math.Asin((2 * Area) / (_a * _b)); }
        }

        public int NumberOfTheSides
        {
            get { return 3; }
        }

        public double[] LengthsOfTheSides
        {
            get { return new double[3] {_a, _b, _c}; }            
        }

        public double I_area
        {
            get { return Area; }
        }
    }
}
