﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    public abstract class Figure
    {
        public virtual double Area { get; set; }
        public virtual double Perimeter { get; set; }
    }
}
