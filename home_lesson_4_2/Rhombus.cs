﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class Rhombus : Figure, ISimplePolygon
    {
        double _d1, _d2 ;
        double _side;

        public Rhombus(double a, double b)
        {
            if (a <= 0 || b <= 0)
                throw new NotPositiveDataFigureException("Sides of figure must have positive lenth only!");            
            else
            {
                _d1 = a;
                _d2 = b;
                _side = Math.Sqrt((_d1 / 2) * (_d1 / 2) + (_d2 / 2) * (_d2 / 2));
            }
        }

         public override double Perimeter
         {             
             get { return (4 * _side); }
         }

         public override double Area
         {
             get { return (_d1 * _d2) / 2; }
         }


         public double Height
         {
             get { return Area / _side; }
         }

         public double Base
         {
             get { return _side; }
         }

         public double AngleBaseAndAdjacentHeight
         {
             get { return Math.Asin(Area / (_side * _side)); }
         }

         public int NumberOfTheSides
         {
             get { return 4; }
         }

         public double[] LengthsOfTheSides
         {
             get { return new double[4] { _side, _side, _side, _side }; }
         }

         public double I_area
         {
             get { return Area; }
         }
    }
}
