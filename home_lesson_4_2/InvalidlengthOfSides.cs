﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class InvalidlengthOfSides : InvalidFigureException
    {
        public InvalidlengthOfSides()
            : base()  { }

        public InvalidlengthOfSides(string message)
            : base(message) { }

        public InvalidlengthOfSides(string message, Exception innerException)
            : base(message, innerException) { }

        public InvalidlengthOfSides(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
