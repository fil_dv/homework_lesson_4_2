﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class Square : Figure, ISimplePolygon
    {
         double _a;

         public Square(double a)
        {
            if (a <= 0)
                throw new NotPositiveDataFigureException("Sides of figure must have positive lenth only!");            
            else
            {
                _a = a;                            
            }
        }

         public override double Perimeter
         {
             get { return (_a * 4); }
         }

         public override double Area
         {
             get { return _a * _a; }
         }

         public double Height
         {
             get { return _a; }
         }

         public double Base
         {
             get { return _a; }
         }

         public double AngleBaseAndAdjacentHeight
         {
             get { return 90; }
         }

         public int NumberOfTheSides
         {
             get { return 4; }
         }

         public double[] LengthsOfTheSides
         {
             get { return new double[4] {_a, _a, _a, _a}; }
         }

         public double I_area
         {
             get { return Area; }
         }
    }
}
