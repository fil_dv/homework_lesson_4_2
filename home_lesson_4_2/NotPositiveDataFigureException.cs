﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class NotPositiveDataFigureException : InvalidFigureException
    {
         public NotPositiveDataFigureException()
            : base()  { }

        public NotPositiveDataFigureException(string message)
            : base(message) { }

        public NotPositiveDataFigureException(string message, Exception innerException)
            : base(message, innerException) { }

        public NotPositiveDataFigureException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
