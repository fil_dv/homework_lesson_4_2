﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    public interface ISimplePolygon
    {
        double Height { get; }
        double Base { get; }
        double AngleBaseAndAdjacentHeight { get; }
        int NumberOfTheSides { get; }
        double[] LengthsOfTheSides { get; }
        double I_area { get; }
        double Perimeter { get; }
    }
}
