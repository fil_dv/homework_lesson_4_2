﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class Circle : Figure, ISimplePolygon
    {
        double _r;

        public Circle(double a)
        {
            if (a <= 0)
                throw new NotPositiveDataFigureException("Sides of figure must have positive lenth only!");           
            else
            {
                _r = a;                            
            }
        }

        public override double Perimeter
         {
             get { return (2 * Math.PI * _r); }
         }

        public override double Area
         {
             get { return (Math.PI * _r * _r); }
         }

        public double Height
        {
            get { return 2 * _r; }
        }

        public double Base
        {
            get { throw new IncorrectValueForTheFigure("Incorrect value for the figure!"); }
        }

        public double AngleBaseAndAdjacentHeight
        {
            get { throw new IncorrectValueForTheFigure("Incorrect value for the figure!"); }
        }

        public int NumberOfTheSides
        {
            get { throw new IncorrectValueForTheFigure("Incorrect value for the figure!"); }
        }

        public double[] LengthsOfTheSides
        {
            get { throw new IncorrectValueForTheFigure("Incorrect value for the figure!"); }
        }

        public double I_area
        {
            get { return Area; }
        }
    }
}
