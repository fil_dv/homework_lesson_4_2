﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class Rectangle : Figure, ISimplePolygon
    {
         double _a, _b;

         public Rectangle(double a, double b)
        {
            if (a <= 0 || b <= 0)
                throw new NotPositiveDataFigureException("Sides of figure must have positive lenth only!");
            else
            {
                _a = a;
                _b = b;                
            }
        }

         public override double Perimeter
         {
             get { return ((_a + _b)*2); }
         }

         public override double Area
         {
             get { return _a * _b; }
         }

         public double Height
         {
             get { return _b; }
         }

         public double Base
         {
             get { return _a; }
         }

         public double AngleBaseAndAdjacentHeight
         {
             get { return 90; }
         }

         public int NumberOfTheSides
         {
             get { return 4; }
         }

         public double[] LengthsOfTheSides
         {
             get { return new double[4] {_a, _b, _a, _b} ; }
         }

         public double I_area
         {
             get { return Area; }
         }
    }
}
