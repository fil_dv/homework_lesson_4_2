﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ISimplePolygon f1 = new Square(3);
                ISimplePolygon f2 = new Rectangle(4, 3);
                ISimplePolygon f3 = new Circle(1);
                ISimplePolygon f4 = new Triangle(2, 2, 1);
                ISimplePolygon f5 = new Rhombus(3, 2);                

                Console.WriteLine("Area of the square = {0}", f1.I_area);
                Console.WriteLine("Area of the rectangle = {0}", f2.I_area);
                Console.WriteLine("Area of the circle = {0}", f3.I_area);
                Console.WriteLine("Area of the triangle = {0}", f4.I_area);
                Console.WriteLine("Area of the rhombus = {0}\n", f5.I_area);
                Console.WriteLine("Sum of these figures = {0}\n", f1.I_area + f2.I_area + f3.I_area + f4.I_area + f5.I_area);

                CompoundFigure h = new CompoundFigure();
                h.add(f1);
                h.add(f2);
                h.add(f3);
                h.add(f4);
                h.add(f5);
                Console.WriteLine("Sum area of the compound figure = {0}\n", h.AllArea);               
            }
            catch (InvalidFigureException e)
            {
                Console.WriteLine(e.Message);
            }            
        }
    }
}
